

import unittest
import json
from app import app

class WaterAPITestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_get_water(self):
        response = self.app.get('/api/water')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data)
        self.assertIn('water', data)

    def test_update_water(self):
        response = self.app.post('/api/water', json={'water': 100})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.data)
        self.assertEqual(data['water'], 100)

    def test_update_water_invalid_value(self):
        response = self.app.post('/api/water', json={'water': -10})
        self.assertEqual(response.status_code, 400)

    def test_update_water_no_json(self):
        response = self.app.post('/api/water')
        self.assertEqual(response.status_code, 400)

    def test_update_water_no_water_key(self):
        response = self.app.post('/api/water', json={})
        self.assertEqual(response.status_code, 400)

if __name__ == '__main__':
    unittest.main()
